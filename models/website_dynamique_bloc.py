# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class WebsiteDynamiqueBloc(models.Model):
    _name = "website.dynamique.bloc"
    _description = "website dynamique bloc"
    _order = "id asc"
    _order = 'sequence, id'

    
    
    name = fields.Char('Name')
    title = fields.Char('Titre')
    title2 = fields.Char('Titre 2')
    description = fields.Html('Description')
    bloc_field_ids = fields.Many2many(comodel_name="bloc", relation="bloc_many_table", column1="bloc_field" )
    sequence = fields.Integer(help="Determine the display order", default=1)

class Bloc(models.Model):
    _name = 'bloc'
    _description = 'Bloc'
    _order = 'sequence, id'

    @api.multi
    def _chlidren_bloc(self):

        all_partners_and_children = {}
        bloc_line_field__ids = []
        result = []
        for lin in self:

            all_partners_and_children= self.search([('parent_id', 'child_of', self.id)]).ids
            print('b ob', all_partners_and_children)
            result.append((0, 0, {'bloc':all_partners_and_children}))
        self.bloc_line_field_id=result


    bloc_field_id = fields.Many2one(comodel_name="website.dynamique.bloc", string="Bloc line", required=False, )
    name = fields.Char()
    title = fields.Char('Titre')
    title2 = fields.Char('Titre 2')
    description = fields.Html('Description')
    image_medium = fields.Binary('Image')
    background = fields.Selection(string="Background", selection=[('image', 'Image'), ('color', 'color'), ], required=False, )
    sequence = fields.Integer(help="Determine the display order", default=1)
    col=fields.Char('Col')
    color=fields.Char('Color')
    lien=fields.Char('Lien')
    parent_id = fields.Many2one(comodel_name="bloc", string="Parent", required=False, )
    bloc_line_field__ids = fields.One2many(comodel_name="bloc.line", inverse_name="bloc_line_field_id", string="", required=False)

class BlocLine(models.Model):
    _name = 'bloc.line'
    _description = 'Bloc Line'
    _order = 'sequence, id'

    bloc_line_field_id = fields.Many2one(comodel_name="bloc", string="Bloc line", required=False, )
    bloc = fields.Many2one(comodel_name="bloc", string="Bloc", required=False, )
    name = fields.Char()
    title = fields.Char('Titre')
    description = fields.Char('Description')
    image_medium = fields.Binary('Image')
    sequence = fields.Integer(help="Determine the display order", default=1)
    col=fields.Char('Col')
    color=fields.Char('Color')
